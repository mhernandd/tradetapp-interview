import { Contact } from './Contact';

export const INITIAL_CONTACTS: Contact[] = [
    { name: 'Patrick' },
    { name: 'Justine' },
    { name: 'Jordan' },
    { name: 'Annie' },
    { name: 'Dustin' }
];