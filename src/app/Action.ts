import { ActionType } from './ActionType';
import { Contact } from './Contact';

export class Action {
    actionType: ActionType;
    contact: Contact;
}