import { Component, OnInit } from '@angular/core';
import { Contact } from '../Contact';
import { INITIAL_CONTACTS } from '../InitialContacts';
import { Action } from '../Action';
import { ActionType } from '../ActionType';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  contacts = INITIAL_CONTACTS;
  actions: Action[] = [];
  undoneActions: Action[] = [];

  removeContact(removedContact: Contact, addToStack = true):void {
    if (addToStack) 
    {
      var action = { actionType: ActionType.Delete, contact: removedContact };
      this.actions.push(action);
    }
    this.contacts = this.contacts.filter(c => c.name !== removedContact.name );
  }

  addContact(newContact: Contact, addToStack = true):void {
    if (addToStack) 
    {
      var action = { actionType:ActionType.Add, contact: newContact};
      this.actions.push(action);
    }
    this.contacts.push(newContact);
  }

  undo():void {
    // If there are any actions that have been done
    if(this.actions.length > 0) {
      // Take last action and push onto list of undone actions
      var lastAction = this.actions.pop();
      this.undoneActions.push(lastAction);
      if (lastAction.actionType === ActionType.Add) {
        this.removeContact(lastAction.contact, false);
      }
      else {
        this.addContact(lastAction.contact, false);
      }
    }
  }

  redo():void {
    // If any actions have been undone
    if(this.undoneActions.length > 0) {
      // last undone action and push it onto stack of done actions
      var lastUndoneAction = this.undoneActions.pop();
      this.actions.push(lastUndoneAction);
      if(lastUndoneAction.actionType === ActionType.Add) {
        this.addContact(lastUndoneAction.contact, false);
      }
      else {
        this.removeContact(lastUndoneAction.contact, false);
      }
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
